cstart:
	@docker-compose -f ./docker-compose.yml up
start:
	-@docker swarm init
	@docker-compose build
	@USER_ID=$$(id -u) docker stack deploy --compose-file ./docker-compose.yml rcg_obvservability
status:
	@docker stack ps rcg_obvservability
stop:
	@docker stack rm rcg_obvservability
clear:
	@sudo rm -rf ./data/**/data/*
