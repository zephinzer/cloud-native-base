#!/bin/sh

echo "---------------------------------";
echo "> nginx configuration as follows:";
cat /etc/nginx/nginx.conf;
echo "/ nginx configuration is as above";
echo "---------------------------------";

echo "starting nginx in the foreground";
nginx-debug -g 'daemon off;';
